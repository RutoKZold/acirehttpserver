import net.winston.acirehttpserver.HttpServer;


public class Main {
    
	public static void main(String[] args) throws InterruptedException {
		HttpServer server = new HttpServer(80);
		server.start();
		while(server.isRunning()){Thread.sleep(1000);}
	}

}
