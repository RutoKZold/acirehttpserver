package net.winston.acirehttpserver.networking;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.Channels;
import java.nio.channels.CompletionHandler;

import net.winston.acirehttpserver.packets.BasePacket;

import com.sun.corba.se.impl.orbutil.closure.Future;

public abstract class BaseTcpServer {
	private int port;
	private AsynchronousServerSocketChannel server;
	private int i = 1;
	private boolean stop = false;
	
	public BaseTcpServer(int port) {
		this.port = port;
	}
	
	public final void start() {
		try {
			server = AsynchronousServerSocketChannel.open().bind(new InetSocketAddress(port));
			server.accept(i++, handler);
		} catch (IOException e) {
			onError(null, e);
		}
	}
	
	private CompletionHandler<AsynchronousSocketChannel, Integer> handler =
			new CompletionHandler<AsynchronousSocketChannel, Integer>() {

				@Override
				public void completed(AsynchronousSocketChannel result,
						Integer attachment) {
					server.accept(i++, handler);
					InputStream is = Channels.newInputStream(result);
					byte[] buffer = new byte[4096];
					int read;
					try {
						while((read = is.read(buffer)) != -1) {		
							byte[] data = new byte[read];
							System.arraycopy(buffer, 0, data, 0, read);
							onReceive(result, data);
						}
						System.out.println("DISCONNECTED");
					} catch (IOException e) {
						onError(result, e);
					}
				}

				@Override
				public void failed(Throwable exc, Integer attachment) {
					// TODO Auto-generated method stub
					
				}
			};
			
	
	public final void stop() {
		try {
			server.close();
		} catch (IOException e) {
			onError(null, e);
		}
		stop = true;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	public boolean isRunning() {
		return !stop;
	}
	
	/**
	 * Processes data that is received from the client
	 * @param client The client that sent the data
	 * @param data The Data to operate
	 */
	public abstract void onReceive(AsynchronousSocketChannel client, byte[] data);
	
	/**
	 * Event for errors captured
	 * @param client The client that the error occurred on, may be null
	 * @param data The exception captured
	 */
	public abstract void onError(AsynchronousSocketChannel client, Exception data);
	
}
