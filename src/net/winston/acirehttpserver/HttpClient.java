package net.winston.acirehttpserver;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;

import net.winston.acirehttpserver.handlers.HttpHandlers;
import net.winston.acirehttpserver.packets.Packet;

public class HttpClient implements IPacketHandler {
	public AsynchronousSocketChannel client;
	public HttpParser parser = new HttpParser(this);
	@Override
	public void process(Packet packet) {
		System.out.println("Command: " + packet.command);
                try{
		client.write(ByteBuffer.wrap(HttpHandlers.getResponse(packet).serialize()));
                }catch(Exception ex) {}
	}
}
