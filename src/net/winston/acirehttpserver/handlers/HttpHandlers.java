package net.winston.acirehttpserver.handlers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/*import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaString;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.JsePlatform;*/

import net.winston.acirehttpserver.HttpStatus;
import net.winston.acirehttpserver.packets.HttpPacket;
import net.winston.acirehttpserver.packets.Packet;
import net.winston.acirehttpserver.tools.Utils;

public class HttpHandlers {
	public static String workingDirectory = "C:\\Users\\1012698\\Documents\\www\\";
	
	public static HttpPacket getResponse(Packet data) {
		String method = data.command.split(" ")[0].toUpperCase();
		HttpPacket response = new HttpPacket();
		response.status = HttpStatus.OK;
		if (method.equals("GET") || method.equals("POST")) {
			String getArgs = extractGetArgs(data.command.split(" ")[1]);
			String postArgs = data.payload.trim();
			String file = getTargetFile(data.command.split(" ")[1]);
			//response.data = execLuaHandler(file, getArgs, postArgs).getBytes();
                        if (isMediaFile(file)) {
                            response.status = HttpStatus.PARTIAL_CONTENT;
                            response.headers.put("Date: ", getGMTDate());
                            response.headers.put("Accept-Range", "bytes");
                            response.headers.put("Content-Type", getContentType(file));
                            File fFile = new File(file);
                            String range = data.headers.get("RANGE");
                            range = range.substring(range.indexOf("bytes=") + 6);
                            long start = Integer.parseInt(range.substring(0, range.indexOf("-")));
                            long end = fFile.length() - 1;
                            try{
                                end = Integer.parseInt(range.substring(range.indexOf("-") + 1));
                            }catch(Exception ex) {}
                            response.headers.put("Content-Range", "bytes " + start + "-" + end + "/" + fFile.length());
                            try {
                                response.data = streamMedia(fFile, start, end);
                            } catch (IOException ex) {
                                System.out.println(ex.toString());
                            }
                        } else {
                            if (method.equals("GET"))
                                    response.data = handleGet(data).getBytes();
                            if (method.equals("POST"))
                                    response.data = handlePost(data).getBytes();
                        }
		} else {
			response.status = HttpStatus.METHOD_NOT_ALLOWED;
			response.data = "<html><head><title>METHOD NOT ALLOWED</title></head><body><h1>This method is not yet supported</h1></body></html>".getBytes();
		}
		return response;
	}
	
	private static String fallbackMessage = "<html><head><title>Error</title></head><body>No file found</body></html>";
	
	private static String getTargetFile(String file) {
		if (file.trim().length() <= 0 || file.trim().equals("/")) {
			file = getDefaultFile();
		}
                if (file.startsWith("/"))
                    file = file.substring(1);
		return workingDirectory + file;
	}
	
	private static String handlePost(Packet packet) {
		String file = packet.command.split(" ")[1];
		if (file.trim().length() <= 0 || file.trim().equals("/")) {
			file = getDefaultFile();
		}
		try {
			return getFile(file);
		} finally {
			return fallbackMessage;
		}
	}
	
	/*private static Globals globals = JsePlatform.standardGlobals();
	private static boolean luaInit = false;
	
	private static String execLuaHandler(String file, String getArgs, String postArgs) {
		if(true)return "";
		if (!luaInit){ 
			globals.get("dofile").call(LuaValue.valueOf("./lua/httphandler.lua"));
			luaInit = true;
		}
		
		return globals.get("handle").call(LuaString.valueOf(file), LuaString.valueOf(getArgs), LuaString.valueOf(postArgs)).tojstring();
	}*/
	
	private static String serializeHeaders(Map<String, String> headers) {
		String output = "";
		for (Map.Entry<String, String> header : headers.entrySet()) {
			output += header.getKey().toUpperCase() + ": " + header.getValue() + "\n";
		}
		return output;
	}
	
	private static String handleGet(Packet packet) {
		String file = packet.command.split(" ")[1];
		if (file.trim().length() <= 0 || file.trim().equals("/")) {
			file = getDefaultFile();
		}
		if (file.startsWith("/") || file.startsWith("\\"))
			file = file.substring(1);
		try {
			return getFile(file);
		} catch(Exception ex) {
			System.out.println(ex.toString());
		}
		return fallbackMessage;
	}
	
	private static String extractGetArgs(String cmd) {
		try{
			return cmd.split("?")[1];
		}catch(Exception ex){ 
			return "";
		}
	}
	
	private static String getFile(String path) throws IOException {
		return new String(Files.readAllBytes(Paths.get(workingDirectory + path)));
	}
        
        private static byte[] streamMedia(File file, long start, long end) throws IOException {
            byte[] bFile = Files.readAllBytes(file.toPath());
            if (bFile.length == (end - start + 1)) return bFile;
            byte[] nF = new byte[(int)end - (int)start + 1];
            System.arraycopy(bFile, (int)start, nF, 0, nF.length);
            return nF;
        }
	
	private static String getDefaultFile() {
		File folder = new File(workingDirectory);
		File[] listOfFiles = folder.listFiles();

	    for (int i = 0; i < listOfFiles.length; i++) {
	    	if (listOfFiles[i].isFile() && listOfFiles[i].getName().toLowerCase().startsWith("index.")) {
	    		return listOfFiles[i].getName();
	    	} 
	    }
	    return null;
	}
        
        private static boolean isMediaFile(String filename) {
            filename = filename.toLowerCase();
            String contentType = getContentType(filename);
            return 
                    contentType.startsWith("video") || 
                    contentType.startsWith("image");
        }
        
        public static String getGMTDate() {
            final java.util.Date currentTime = new java.util.Date();

            final SimpleDateFormat sdf =
                    new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss a z");

            // Give it to me in GMT time.
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            return sdf.format(currentTime);
        }
        
        private static String getContentType(String file) {
            try {
                return Files.probeContentType(Paths.get(file));
            } catch (IOException ex) {
                System.out.println(ex.toString());
            }
            return "text/html";
        }
}
