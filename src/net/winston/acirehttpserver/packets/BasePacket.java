package net.winston.acirehttpserver.packets;

import java.util.HashMap;
import java.util.Map;

public abstract class BasePacket {	
	public abstract byte[] serialize();
}
