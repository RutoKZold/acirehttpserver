package net.winston.acirehttpserver.packets;

import java.util.HashMap;
import java.util.Map;

import net.winston.acirehttpserver.HttpStatus;
import net.winston.acirehttpserver.tools.Utils;

public class HttpPacket extends BasePacket {
	public HttpStatus status;
	public double httpVersion = 1.1;
	public String contentType = "text/html";
	public Map<String, String> headers =
			new HashMap<String, String>();
	public byte[] data;
	
	@Override
	public byte[] serialize() {
		String serialized = "HTTP/" + httpVersion + " " + status.code + " " + status.text + "\r\n";
		if (data != null && !headers.containsKey("Content-Length")) {
			headers.put("Content-Length", data.length + "");
		}
		if (!headers.containsKey("Content-Type")) {
			headers.put("Content-Type", contentType);
		}
		for(Map.Entry<String, String> header : headers.entrySet()) {
			serialized += header.getKey() + ": " + header.getValue() + "\r\n";
		}
		serialized += "\r\n";
		if (data != null)
			return Utils.combine(serialized.getBytes(), data);
		return serialized.getBytes();
	}

}
