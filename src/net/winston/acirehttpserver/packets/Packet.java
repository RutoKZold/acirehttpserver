package net.winston.acirehttpserver.packets;

import java.util.HashMap;
import java.util.Map;

public class Packet extends BasePacket {
	public String command;
	public Map<String, String> headers = new HashMap<String, String>();
	public String payload;

	@Override
	public byte[] serialize() {
		String cmd = command + "\r\n";
		if (payload != null && payload.length() > 0 && !headers.containsKey("Content-Length")) {
			headers.put("Content-Length", payload.length() + "");
		}
		for(Map.Entry<String, String> header : headers.entrySet()) {
			cmd += header.getKey() + ": " + header.getValue() + "\r\n";
		}
		cmd += "\r\n";
		return (cmd + payload).getBytes();
	}

	public void clear() {
		command = "";
		headers.clear();
		payload = "";
	}

}
