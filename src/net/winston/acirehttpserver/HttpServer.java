package net.winston.acirehttpserver;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

import net.winston.acirehttpserver.handlers.HttpHandlers;
import net.winston.acirehttpserver.networking.BaseTcpServer;
import net.winston.acirehttpserver.packets.HttpPacket;

public class HttpServer extends BaseTcpServer {
	
	public HttpServer(int port) {
		super(port);
	}

	@Override
	public void onReceive(AsynchronousSocketChannel client, byte[] data) {
		String content = new String(data, Charset.forName("UTF-8"));
		HttpClient hClient = new HttpClient();
		hClient.client = client;
		try {
			hClient.parser.process(content);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onError(AsynchronousSocketChannel client, Exception data) {
		System.out.println("Error occured");
	}

}
