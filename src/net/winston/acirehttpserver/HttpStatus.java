package net.winston.acirehttpserver;

public enum HttpStatus {
	ACCEPTED(202, "Accepted"),
	BAD_GATEWAY(502, "Bad Gateway"),
	BAD_REQUEST(400, "Bad Request"),
	CONFLICT(409, "Conflict"),
	CONTINUE(100, "Continue"),
	CREATED(201, "Created"),
	EXPECTATION_FAILED(417, "Expectation Failed"),
	FAILED_DEPENDENCY(424, "Failed Dependency"),
	FORBIDDEN(403, "Forbidden"),
	GATEWAY_TIMEOUT(504, "Gateway Timeout"),
	GONE(410, "Gone"),
	HTTP_VERSION_NOT_SUPPORTED(505, "HTTP VERSION NOT SUPPORTED"),
	INSUFFICIENT_STORAGE(507, "Insufficient Storage"),
	INTERNAL_SERVER_ERROR(5000, "Server Error"),
	LENGTH_REQUIRED(411, "Length Required"),
	LOCKED(423, "Locked"),
	METHOD_NOT_ALLOWED(405, "Method Not Allowed"),
	MOVED_PERMANENTLY(301, "Moved Permanently"),
	MOVED_TEMPORARILY(302, "Moved Temporarily"),
	MULTI_STATUS(207, "Multi-Status"),
	MULTIPLE_CHOICES(300, "Multiple Choices"),
	NO_CONTENT(204, "No Content"),
	NON_AUTHORITATIVE_INFORMATION(203, "Non Authoritative Information"),
	NOT_ACCEPTABLE(406, "Not Acceptable"),
	NOT_FOUND(404, "Not Found"),
	NOT_IMPLEMENTED(501, "Not Implemented"),
	NOT_MODIFIED(304, "Not Modified"),
	OK(200, "Ok"),
	PARTIAL_CONTENT(206, "Partial Content"),
	PAYMENT_REQUIRED(402, "Payment Required"),
	PRECONDITION_FAILED(412, "Precondition Failed"),
	PROCESSING(102, "Processing"),
	PROXY_AUTHENTICATION_REQUIRED(407, "Proxy Authentication Required"),
	REQUEST_TIMEOUT(408, "Request Timeout"),
	REQUEST_TOO_LONG(413, "Request Entity Too Long"),
	REQUEST_URI_TOO_LONG(414, "Request-URI Too Long"),
	REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested Range Not Satisfiable"),
	RESET_CONTENT(205, "Reset Content"),
	SEE_OTHER(303, "See Other"),
	SERVICE_UNAVAILABLE(503, "Service Unavailable"),
	SWITCHING_PROTOCOLS(101, "Switching Protocols"),
	TEMPORARY_REDIRECT(307, "Temporary Redirect"),
	UNAUTHORIZED(401, "Unauthorized"),
	UNPROCESSABLE_ENTITY(422, "Unprocessable Entity"),
	UNSUPPORTED_MEDIA_TYPE(415, "Unsupported Media Type"),
	USE_PROXY(305, "Use Proxy")
	;
	
	HttpStatus(int code, String text) {
		this.code = code;
		this.text = text;
	}
	public int code;
	public String text;
}
