package net.winston.acirehttpserver.tools;

public class Utils {
	public static byte[] combine(byte[] a1, byte[] a2) {
		byte[] res = new byte[a1.length + a2.length];
		System.arraycopy(a1, 0, res, 0, a1.length);
		System.arraycopy(a2, 0, res, a1.length, a2.length);
		return res;
	}
}
