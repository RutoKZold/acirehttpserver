package net.winston.acirehttpserver;

import net.winston.acirehttpserver.packets.Packet;

public interface IPacketHandler {
	void process(Packet packet);
}
