package net.winston.acirehttpserver;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import net.winston.acirehttpserver.handlers.HttpHandlers;
import net.winston.acirehttpserver.packets.Packet;

/**
 * Created by Winston on 8/17/13.
 */
public class HttpParser {

    private Packet _parPacket = new Packet();
    private int _contentLength = 0;
    private String _rawData = "";
    private boolean _headersParsed;
    private IPacketHandler handler;
    
    public HttpParser(IPacketHandler handler) {
    	this.handler = handler;
    }

    public void process(String data) throws IOException, NoSuchAlgorithmException {
        _rawData += data;
        tryParse();
    }

    private void tryParse() throws IOException, NoSuchAlgorithmException {
        if (!_headersParsed) {
            int index = _rawData.indexOf("\r\n\r\n");
            if (index == -1) {
                return;
            }
            parseHeaders(_rawData.substring(0, index));
            _rawData = _rawData.substring(index + 4);
        }
        if (_rawData.length() == _contentLength) {
            _parPacket.payload = _rawData;
            collect();
        }
        else if (_rawData.length() >= _contentLength) {
            _parPacket.payload = _rawData.substring(0, _contentLength);
            collect();
            tryParse();
        }
    }

    private void parseHeaders(String strheaders) {
        String cmd = "";
        String[] lines = strheaders.split("\r\n");
        Map<String, String> headers = new HashMap<String, String>();
        int lineCount = lines.length;
        for (int i = 0; i < lineCount; i++) {
            if (i == 0) {
                cmd = lines[i].toUpperCase();
            } else {
                if (lines[i] == null || lines[i].trim() == "") continue;
                int lnindex = lines[i].indexOf(':');
                if (lnindex > -1) {
                    String key = lines[i].substring(0, lnindex).trim().toUpperCase();
                    String value = lines[i].substring(lnindex + 2).trim();
                    headers.put(key, value);
                }
            }
        }
        _parPacket.headers = headers;
        _parPacket.command = cmd;;
        if (_parPacket.headers.containsKey("CONTENT-LENGTH")) {
            _contentLength = Integer.parseInt(_parPacket.headers.get("CONTENT-LENGTH"));
        }
        _headersParsed = true;
    }

    private void collect() throws IOException, NoSuchAlgorithmException {

        if (_parPacket.command.equals("")){
            return;
        }

        handler.process(_parPacket);

        _headersParsed = false;

        _parPacket.clear();

        if (_contentLength > 0) {

            _rawData = _rawData.substring(0, _contentLength);
            _contentLength = 0;
        }
    }

}

